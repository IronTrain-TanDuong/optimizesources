import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { GlobalVariables } from '../../global/global_variable';

@Component({
  selector: 'page-popup-rating',
  templateUrl: 'popup-rating.html',
})
export class PopupRatingPage {
  private itemsCollection: AngularFirestoreCollection<any>;
  user: any = GlobalVariables.user;
  ratingCheck: number = 0;
  userHomestay: any;
  selectedArray: any = [];
  testList: any = [
    { testID: 1, testName: " INTERNET GAME", checked: false },
    { testID: 2, testName: " NIGHT LIFE", checked: false },
    { testID: 3, testName: "PET", checked: false },
    { testID: 4, testName: "MUSIC", checked: false },
    { testID: 5, testName: "MOVIE", checked: false },
    { testID: 6, testName: "TRIP", checked: false },
    { testID: 7, testName: "SPORTS", checked: false },
    { testID: 8, testName: "FOOD", checked: false },
    { testID: 9, testName: "SHOPPING", checked: false }

  ]
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private util: UtilProvider,
    private db: AngularFirestore
  ) {

    this.userHomestay = this.navParams.get('userHomestay');

  }
  ionViewDidLoad() {
    this.itemsCollection = this.db.collection<any>('homestays');
  }
  clickStar(number) {
    this.ratingCheck = number;
  }
  clickSend() {
    var userRef = this.itemsCollection.doc(this.userHomestay['base64']);
    userRef.collection("rating").doc(this.user['base64']).set({ Star: this.ratingCheck });
    this.viewCtrl.dismiss();
  }
  clickClose() {
    this.viewCtrl.dismiss();
  }

  checkAll() {
    for (let i = 0; i <= this.testList.length; i++) {
      this.testList[i].checked = true;
    }
  }

  selectMember(data) {
    if (data.checked == true) {
      this.selectedArray.push(data);
    } else {
      let newArray = this.selectedArray.filter(function (el) {
        return el.testID !== data.testID;
      });
      this.selectedArray = newArray;
    }
  }
}
